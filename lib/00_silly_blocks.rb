def reverser
  yield.split.map(&:reverse).join " "
end

def adder(number = 1)
  yield + number
end

def repeater(times_to_repeat = 1)
  times_to_repeat.times { yield }
end
