def measure(times = 1)
  beginning_time = Time.now
  times.times { yield }
  (Time.now - beginning_time) / times
end
